#!/usr/bin/env node

const fs = require('fs')
const path = require('path')

// find the object with the most keys and return its index
const findLargestObjIndex = (arr) => {
  let max = 0
  let index = 0
  let len = 0
  arr.forEach((obj, i) => {
    len = Object.values(obj).length
    if (len > max) {
      max = arr.length
      index = i
    }
  })
  return index
}

// based on: https://stackoverflow.com/a/64928976/7190049
const ObjectsToCsv = (arr) => {
  // set the keys of the csv the keys of the object with the most keys in the array
  // such that no column "spills" to the next row
  const index = findLargestObjIndex(arr)
  const array = [Object.keys(arr[index])].concat(arr)
  return array.map(row => {
    return Object.values(row).map(value => {
      return typeof value === 'string' || typeof value === 'object' ? `"${JSON.stringify(value).replace(/"/g, "")}"` : value
    }).toString()
  }).join('\n')
}

const filePath = process.argv[2]
if (filePath == null) {
  console.error('Input file must be specified')
  process.exit(-1)
}
if (!filePath.endsWith('.json')) {
  console.error('Input file must be a .json file')
  process.exit(-1)
}

/* 
json file format:  
{
  item1: {
    id1: {
      ..
    },
    id2: {
      ..
    }
  },
  ..
  item100: {
    ..
  }
}
*/
try {
  if (fs.existsSync(filePath)) {
    let jsonFile = fs.readFileSync(filePath);
    jsonFile = JSON.parse(jsonFile)
    let fileArr = filePath.split(path.sep)

    for (const [k,v] of Object.entries(jsonFile)) {
      const keys = Object.keys(v)
      let items = Object.values(v).map((item, i) => {
        item.id = keys[i]
        return item
      })
      // sort
      const l = items.length
      for (let i = 0; i < l ; i++) {
        items.push(Object.fromEntries(Object.entries(items.splice(0,1)[0]).sort()))
      }
      const csv = ObjectsToCsv(items)
      fileArr.pop()
      fileArr.push(k)
      const name = `${fileArr.join(path.sep)}.csv`
      fs.writeFileSync(name, csv)
      console.log(name + " saved successfully")
    }
  }
} catch(err) {
  console.error(err)
  process.exit(-1)
}