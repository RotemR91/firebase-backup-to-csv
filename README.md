# Firebase backup to CSV

A Node.js cli helper tool to transform firebase JSON backup to multiple CSV files (one for each collection)

## Purpose

* Migration from Firebase to Supabase: Supabase allows users to upload a csv tables.
* Firebase allows documents in a collection to have different structures. 
  Example: 
  ```
  [Collection]: {
    abcde: {
      name: ..,
      text: ..,
      updatedAt: ..,
    },
    fghi: {
      name: ..,
      text: ..,
      updatedAt: ..,
      optional: ..
    }
  }
  ```
  Which can cause problems for csv files. The tool takes the document with the largest number of keys and uses these keys as the header fo the csv file.
* **Remark**: Of course, if the data in each of your documents has a complete different structure you can't export it in csv format because columns will "leak" down rows.

## Requirement

* JSON file format:
  ```
  {
    collection1: {
      id1: {
        ..
      },
      id2: {
        ..
      }
    },
    ..
    collection100: {
      ..
    }
  }
  ```

## Usage

`npx firebase-backup-to-csv <path_to_json_backup>`
Will generate all the csv files in the same folder where the json backup is located.

## Helpers

1. Usually Firebase fetches each collection as an array of objects, where each object is a document with the id as a hidden property (which won't get exported):
```
[
  {
    id: <hidden>
    ..
  },
  {
    ..
  }
]
```
Therefore, it's necessary to transform each collection to be an object of objects:

```js
let newBlogPostsCollection = {}
// blogPostsCollection is an array of objects
blogPostsCollection.forEach((document, i) => {
 const id = document.id
 delete document.id
 newBlogPostsCollection[id] = {...document}
})
.
.
.
const collections = {blogPosts: blogPostsCollection, ...}
```

2. Firebase in blaze plan doesn't allow users to automatically generate backups,therefore we will export `collections` as a json file:

```js
const collectionsData = new Blob([collections], { type: "application/json" })
const csvUrl = URL.createObjectURL(collectionsData)

const hiddenElement = document.createElement("a")
hiddenElement.href = csvUrl
hiddenElement.target = "_blank"
hiddenElement.download = "backup.json"
hiddenElement.click()
```
   